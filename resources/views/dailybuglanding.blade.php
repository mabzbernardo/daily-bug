<!DOCTYPE html>
<html>
<head>
	<title>Daily Bug</title>
	 <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="d-flex vh-100">
		<div class="d-flex vh-100 w-50 align-items-center justify-content-end" style="background-color: #eeaeca; color: #2e5a8e">
			<h1>DAILY</h1>
		</div>
		<div class="d-flex vh-100 w-50 align-items-center" style="background-color: #2e5a8e; color: #eeaeca">
			<h1>BUG</h1>
		</div>
	</div>
</body>
</html>