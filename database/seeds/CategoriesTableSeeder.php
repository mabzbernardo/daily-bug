<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();

        \DB::table('categories')->insert(array(
        	0 => 
        	array (
        		'id' => 1,
        		'name' => 'HTML',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	1 => 
        	array (
        		'id' => 2,
        		'name' => 'CSS',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	2 => 
        	array (
        		'id' => 3,
        		'name' => 'Javascript',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	3 => 
        	array (
        		'id' => 4,
        		'name' => 'MySQL',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	4 => 
        	array (
        		'id' => 5,
        		'name' => 'PHP',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	5 => 
        	array (
        		'id' => 6,
        		'name' => 'Laravel',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        ));
    }
}
